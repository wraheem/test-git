// Set up Nitro     oooooo    
var connectionParams={};
connectionParams.apiKey = '6bd0d682410346ccb5e137b09edda88e';
connectionParams.server = 'http://sprinttrial.nitro.bunchball.net/nitro/json';
//connectionParams.server = "http://sandbox.bunchball.net/nitro/json/";

/// Create GUID for userID //////
function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
};

function guid() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}
////////////////////////////////

var userId = "";

// look up sprint cookie
// if cookie exists, then try to get progress for the user
userId =  readCookie('DYN_USER_ID');


// Get userID from the bunchball cookie. This userId is the one we want
// because we can pull previous progress.
var bunchBallCookie = readCookie("bunchBallBeta");
if(bunchBallCookie){
    userId = bunchBallCookie;
    console.log("User ID  from the bunchBallBeta cookie is: " + bunchBallCookie);
}


// If we found a userId, the set it in Nitro
// else generate a new one, assign it to the cookie
if(userId){
    console.log("We have the userId from the cookie");
    connectionParams.userId = userId;
}
else{
    // no cookie.
    // TODO: Lookup FB ID?
    console.log("We have to generate a new userId.");
    // Generate an id
    var guid = guid();
    connectionParams.userId = guid;
    //no cookie? create it!
    console.log("Creating the cookie");
    setCookie("bunchBallBeta", guid, 60);
    userId = guid;
    console.log("User ID  is: " + userId);
}


// Check query parameter to see if user came from newsletter /////////////
var newsletter = false;
var GET = {};
var q = window.location.search;
function query(input) {
    'use strict';
    if (input.length > 1) {
        var param = input.slice(1).replace(/\+/g, ' ').split('&'),
            plength = param.length,
            tmp,
            p;

        for (p = 0; p < plength; p += 1) {
            tmp = param[p].split('=');
            GET[decodeURIComponent(tmp[0])] = decodeURIComponent(tmp[1]);
        }
    }
};

query(q);

// Set newsletter to true if the user comes from the newsletter
if(GET['q'] === "ECID"){
    console.log("Came from newsletter");
    newsletter = true;
}
// End of newsletter check///////////



// Now that we have a userId, actually init Nitro
connectionParams.debug = true;
var nitro = new Nitro(connectionParams);
var sessionKey;


//console.log("Session key is " + sessionKey);

function getUserProgress(){
    // Get the user's progress. Pass the result on to the renderBadgeBar function.
    //https://main.nitro.bunchball.net/nitro/json?apiVersion=4%2E4&tags=signUpForNewsletter&returnCount=0&showOnlyTrophies=false&method=user%2EgetChallengeProgress&startIndex=0&folder=&showCanAchieveChallenge=false&asyncToken=&showRulesProgress=true&challengeName=
    nitro.callAPI("method=user.getChallengeProgress&returnCount=0&showOnlyTrophies=false&startIndex=0&folder=&showCanAchieveChallenge=false", "renderBadgeBar");
    sessionKey = nitro.connectionParams.sessionKey;
    console.log("session key:"  + sessionKey );
}


// Here we take the result back from the user.getChallengeProgress and go through each
function renderBadgeBar(data){
    //Check to see if there's an error first
    if (data.Nitro.res == 'err') {
        // handle errors here
        var errorCode = data.Nitro.Error.Code;
        var errorMessage = data.Nitro.Error.Message;
        console.log("Error getting the user's progress: " + errorMessage);
    }
    else{
        console.log("Rendering Challenge Track");
        // console.log(data.Nitro);
        var challenges = data.Nitro.challenges.Challenge;
        //console.log(challenges);
        for (var key in challenges)
        {
         //console.log(challenges[key].name);
         switch(challenges[key].name)
            {
            case "downloadApp":
                // If user has downloaded the app
                if(challenges[key].completionCount > 0){
                    console.log("Update Challenge Track with Download Nascar App");
                    /*var b1 = document.getElementById("badgeTwo");
                     b1.style.backgroundColor = "green";*/
                    //Update the tile
                    //$('.challenge3').hide();
                    $('.challenge3').hide();
                    $('.challenge3-done').show().animate({opacity: 1}, 125);
                    $('#game-progress .turn3').attr('data-complete', 'true');
                    //calls into home.js (initTrack)
					Gamification.initTrack();
                }
                break;
            case "signUpForNewsletter":
                //console.log("In newsletter.");
                //console.log(challenges[key]);
                // If user has already signed up from newsletter OR is actually coming from the newsletter
                if(challenges[key].completionCount > 0 || newsletter){
                    console.log("Update Challenge Track with Sign up for newsletter");
                    // Update the track.
                    /*var b1 = document.getElementById("badgeThree");
                     b1.style.backgroundColor = "green";*/
                    // Update the tile
                    $('.challenge1').hide();
                    $('.challenge1-done').show().animate({opacity: 1}, 125);
                    $('#game-progress .turn1').attr('data-complete', 'true');
                    //calls into home.js (initTrack)
                    // test call notification
                    //nitro.showPendingNotifications();
					Gamification.initTrack();
                    
                }
                // If the user hasn't completed the newsletter task BUT came from the newsletter
                if(challenges[key].completionCount == 0 && newsletter){
                    console.log("Update Challenge Track with Sign up for newsletter and updating Nitro challenge");
                    // Update the user for newsletter
                    signUpForNewsletter();
                    // Update the tile
                    $('.challenge1').hide();
                    $('.challenge1-done').show().animate({opacity: 1}, 125);
                    $('#game-progress .turn1').attr('data-complete', 'true');
                    //calls into home.js (initTrack)
					Gamification.initTrack();
                }
                break;
            case "sharePageWithFriends":
                // User has shared page on Facebook or Twitter
                if(challenges[key].completionCount > 0){
                    console.log("Update Challenge Track with Share Page with Friends");
                    /*var b1 = document.getElementById("badgeThree");
                     b1.style.backgroundColor = "green";*/
                    //Update the tile
                    $('.challenge4').hide();
                    $('.challenge4-done').show().animate({opacity: 1}, 125);
                    $('#game-progress .turn4').attr('data-complete', 'true');
                    //calls into home.js (initTrack)
                    Gamification.initTrack();
                }
                break;
            case "followMSC":
                // User has confirmed that they follow MSC on Facebook or Twitter
                if(challenges[key].completionCount > 0){
                    console.log("Update Challenge Track with Follow MSC");
                    /*var b1 = document.getElementById("badgeFive");
                     b1.style.backgroundColor = "green";*/
                    //Update the tile
                    $('.challenge2').hide();
                    $('.challenge2-done').show().animate({opacity: 1}, 125);
                    $('#game-progress .turn2').attr('data-complete', 'true');
                    //calls into home.js (initTrack)
                    Gamification.initTrack();
				
                }
            }
        }
    }
    
}


// Function to set the cookie
// c_name: name of the cookie
// value: Value of the cookie
// exdays: Days until the cookie expires
function setCookie(c_name,value,exdays)
{
    var exdate=new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
    document.cookie=c_name + "=" + c_value;
}



// Array used to store the action feed (newsfeed)
var actionFeed = new Array();
// this is the action feed.
function getActionFeed(){
    var url = "method=site.getActionFeed&apiKey=6bd0d682410346ccb5e137b09edda88e&returnCount=20";
    nitro.callAPI(url, "updateActionFeed");
    sessionKey = nitro.connectionParams.sessionKey;
}

// Callback function to parse actionFeed into array
function updateActionFeed(data){
    console.log("Action feed");
    //console.log(data);
    var feed = data.Nitro.items.item;
    //console.log(challenges);
    for (var key in feed)
    {
        console.log(feed[key].content);
        if(feed[key].content.length != 0){
            actionFeed.push(feed[key].content);
        }
    }
    console.log(actionFeed);
    Gamification.initActionFeed(actionFeed);
}


getActionFeed();
getUserProgress();
// For maybe later
//getPendingNotifications();




//////////////////////////////////////////////////////////////////////////////////////////
// These are the callback function for the data returned from Nitro user.LogAction methods
//
//
// Callback from nitro will come here and update the progress track for the newsletter
function updateProgressBarWithNewsletter(data) {
    console.log(data.Nitro);
    if(data.Nitro.res == "ok"){
        console.log("Let's update the track with turn one.");
        //console.log(nitro.showPendingNotifications());
    }
    else{
        console.log("There was a problem updating the user's progress");
    }
}

// Callback from nitro will come here and update the progress track for the Foolw MSC
function updateProgressBarWithFollowMSC(data) {
    console.log(data.Nitro);
    if(data.Nitro.res == "ok"){
        console.log("Let's update the track with turn two.");
        //$('.challenge2').hide();
        //$('.challenge2-done').show().animate({opacity: 1}, 125);
        $('#game-progress .turn2').attr('data-complete', 'true');
        Gamification.initTrack();
        //console.log(nitro.showPendingNotifications());
		Gamification.mscComplete();
    }
    else{
        console.log("There was a problem updating the user's progress");
    }
}

// Callback from nitro will come here and update the progress track for the AppDaownload
function updateProgressBarAppDownload(data) {
    console.log(data.Nitro);
    if(data.Nitro.res == "ok"){
        console.log("Let's update the track with turn three.");
        $('.challenge3').hide();
        $('.challenge3-done').show().animate({opacity: 1}, 125);
        $('#game-progress .turn3').attr('data-complete', 'true');
        Gamification.initTrack();
    }
    else{
        console.log("There was a problem updating the user's progress");
    }
}

// Callback from nitro will come here and update the progress track for the Facebook or Twitter share page
function updateProgressBarSharePage(data) {
    console.log(data.Nitro);
    if(data.Nitro.res == "ok"){
        console.log("Let's update the track with turn four.");
        $('.challenge4').hide();
        $('.challenge4-done').show().animate({opacity: 1}, 125);
        $('#game-progress .turn4').attr('data-complete', 'true');
        Gamification.initTrack();
    }
    else{
        console.log("There was a problem updating the user's progress");
    }
}







////////////////////////////
// user.logAction functions
// These are the functions that call Nitro when a user completes tasks
// url consists of:
// the method being called: user.logAction
// the userId. This is set earlier
// The newsfeed. This is supposed to be URL encoded html. That doesn't work. Sending regular text for now.
// The session key: Also set earlier

// Supposed to use URL encoded text for newsfeed. Didn't work
//'%3Cspan%3EAnother%20fan%20just%20completed%20turn%20%231%3C%2Fspan%3E'

//Turn one
function signUpForNewsletter(){
    sessionKey = nitro.connectionParams.sessionKey;
    var url = "method=user.logAction&userid=" + userId + "&returnCount=20&tags=Sign_Up_For_Newsletter&newsfeed=" +
        'Fans Viewed Inside Lane' + "&sessionKey=" + sessionKey;
    nitro.callAPI(url, "updateProgressBarWithNewsletter");
}

//Turn two
function followedMissSprintCup(){
    sessionKey = nitro.connectionParams.sessionKey;
    var url = "method=user.logAction&userid=" + userId + "&returnCount=20&tags=Follow_MSC&newsfeed=" +
        'Another fan just completed turn %232' + "&sessionKey=" + sessionKey;
    nitro.callAPI(url, "updateProgressBarWithFollowMSC");
}

//Turn three
function downloadApp(){
    sessionKey = nitro.connectionParams.sessionKey;
    console.log("newsletter");
    var url = "method=user.logAction&userid=" + userId + "&returnCount=20&tags=downloadApp&newsfeed=" +
        'Another fan just completed turn %233' + "&sessionKey=" + sessionKey;
    nitro.callAPI(url, "updateProgressBarAppDownload");
}

// Turn four
function sharePageWithFriends(){
    sessionKey = nitro.connectionParams.sessionKey;
    var url = "method=user.logAction&userid=" + userId + "&returnCount=20&tags=sharePageWithFriends&newsfeed=" +
        'Another fan just completed turn %232' + "&sessionKey=" + sessionKey;
    nitro.callAPI(url, "updateProgressBarSharePage");
}
// End of user.logAction functions/////









///////////////////////////////
// Older test functions

function processResult(data, token) {
    // The result is always returned as "ok" or "err" when an error occurs with the request.
    if (data.Nitro.res == 'err') {
        // handle errors here
        var errorCode = data.Nitro.Error.Code;
        var errorMessage = data.Nitro.Error.Message;
    } else {
        balance = data.Nitro.Balance.points;
        var elem = document.getElementById("bbpoints");
        elem.innerHTML = "The user with id <i>WidgetUser1</i> has " + balance + " points.";
    }
}

function showNotification(data){
    console.log("Showing Notification");
    console.log(data);
}


function getPendingNotifications(){
    var url = "method=user.getPendingNotifications";
    nitro.callAPI(url, "showNotification");
}
function userRepliedRetweetedMSC(){
    //retweetOrReplyMSC
}

function downloadAppleNascarApp(){
    nitro.callAPI("method=user.awardChallenge&userid=1234&challenge=downloadNascarApp", "processResult1");
}

function downloadAndroidNascarApp(){
    nitro.callAPI("method=user.awardChallenge&userid=1234&challenge=downloadNascarApp", "processResult1");
}
function getBalance(){
    //nitro.callAPI("method=user.creditPoints&userId=1234&points=100", "processResult1");
    nitro.callAPI("method=user.getPointsBalance", "processResult");
}

function addPoints(points){
    var call = "method=user.creditPoints&userId=1234&points=" + points;
    nitro.callAPI(call, "processResult1");
    nitro.callAPI("method=user.getPointsBalance", "processResult");
}

function subPoints(points){
    var call = "method=user.debitPoints&userId=1234&points=" + points;
    nitro.callAPI(call, "processResult1");
    nitro.callAPI("method=user.getPointsBalance", "processResult");
}



function followedMissSprintCupOnFacebook(){
    nitro.callAPI("method=user.awardChallenge&userId=1234&challenge=facebookLikeThePage", "updateBadgeBar");
}



function deletePoints(data, token){
    if (data.Nitro.res == 'err') {
        var errorCode = data.Nitro.Error.Code;
        var errorMessage = data.Nitro.Error.Message;
        console.log(errorCode);
    }
    //console.log(data);
    var points = data.Nitro.Balance['points'];
    subPoints(points);
}

function resetPoints(){
    console.time("Resesting points");
    nitro.callAPI("method=user.getPointsBalance", "deletePoints");
    console.timeEnd("Ressting points");

}


function updateBadgeBar(data){
    //data.Nitro.
    console.log(data.Nitro.Achievements.challengesAchieved.ChallengeAchieved.name);
}

function getRecentActions(){
    nitro.callAPI("method=site.getRecentActions&returnCount=8&sessionKey="+ sessionKey, "parseRecentActions");
}

function parseRecentActions(data){
    console.log(data);
    console.log("Recent Actions");
}

function fillLeaderBoard(data){
    // get the user; fake user guest1234
    // call the api to get the current top 5? leaders
    // if the user isn't in the top five, replace the fifth one with the user

    console.log(data.Nitro.leaders.Leader[0].points);
    var player1 = data.Nitro.leaders.Leader[0].points / 200;
    $( "#progressbar" ).progressbar({
        value: 85
    });

    $( "#progressbar1" ).progressbar({
        value: 37
    });
}

function getCookie(c_name)
{
    var c_value = document.cookie;
    var c_start = c_value.indexOf(" " + c_name + "=");
    if (c_start == -1)
    {
        c_value = null;
        //no cookie? create it!
        console.log("Creating the cookie");
        setCookie("bunchBallBeta", guid, 60);
    }
    else
    {
        c_start = c_value.indexOf("=", c_start) + 1;
        var c_end = c_value.indexOf(";", c_start);
        if (c_end == -1)
        {
            c_end = c_value.length;
        }
        c_value = unescape(c_value.substring(c_start,c_end));
    }
    return c_value;
}



function getPointLeaders(){
    console.log("getting point leaders");
    //nitro.callAPI("method=site.getPointsLeaders&returnCount=5", "knockData");

    var url = "http://sandbox.bunchball.net/nitro/json/?method=site.getPointsLeaders&returnCount=5&jsCallback=oneTwo&sessionKey="
        + sessionKey + "?jsoncallback=?";
    console.log(url);
    /*
     $.getJSON(url, function(data){
     console.log(data);
     });
     */
}


// Old unused code:

/*nitro.callAPI("method=user.getChallengeProgress&challengeName=downloadNascarApp", "renderBadgeBar");
 nitro.callAPI("method=user.getChallengeProgress&challengeName=signUpForNewsletter", "renderBadgeBar");
 nitro.callAPI("method=user.getChallengeProgress&challengeName=facebookLikeThePage", "renderBadgeBar");*/
//nitro.callAPI("method=user.getChallengeProgress&challengeName=tweetThePage", "renderBadgeBar");
// A challenge for the user reweeting replying ten times
//nitro.callAPI("method=user.getChallengeProgress&challengeName=repliedRetweeted10Times", "renderBadgeBar");
//nitro.callAPI("method=batch.run&methodFeed=['method=user.getChallengeProgress&userId=1234']", "renderBadgeBar");



/*
 console.log("Test notifications");
 nitro.showPendingNotifications();
 console.log("End Test notifications");
 */
//getPointLeaders();



